/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_pix.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <adrvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 17:42:47 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 15:58:19 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	print_pix(t_env *e)
{
	int x;

	x = (int)e->x * 4 + (int)e->y * (int)e->a.size_line;
	if (x < 0 || x > WIN_X * WIN_Y * 4)
		return ;
	e->a.data[x] = e->color & 0xff;
	e->a.data[x + 1] = e->color >> 8 & 0xff;
	e->a.data[x + 2] = e->color >> 16 & 0xff;
}

void	print_pix_mm(t_env *e)
{
	int x;
	int color;

	color = e->mm.color;
	x = e->mm.x * 4 + e->mm.y * e->a.size_line;
	if (x < 0 || x > WIN_X * WIN_Y * 4 || x < e->mm.y * e->a.size_line
			|| x >= (e->mm.y + 1) * e->a.size_line)
		return ;
	e->a.data[x] = color & 0xff;
	e->a.data[x + 1] = color >> 8 & 0xff;
	e->a.data[x + 2] = color >> 16 & 0xff;
}

void	print_pix_ad(t_env *e)
{
	int x;

	e->ad.color = 0xff0000;
	x = e->ad.x * 4 + e->ad.y * e->a.size_line;
	if (x < 0 || x > WIN_X * WIN_Y * 4 || x < e->ad.y * e->a.size_line
			|| x >= (e->ad.y + 1) * e->a.size_line)
		return ;
	e->a.data[x] = e->ad.color & 0xff;
	e->a.data[x + 1] = e->ad.color >> 8 & 0xff;
	e->a.data[x + 2] = e->ad.color >> 16 & 0xff;
}

void	print_pix_wall(int x, int y, int color, t_env *e)
{
	int wow;

	wow = x * 4 + y * e->a.size_line;
	if (wow < 0 || wow > WIN_X * WIN_Y * 4 || wow < y * e->a.size_line
			|| wow >= (y + 1) * e->a.size_line)
		return ;
	e->a.data[wow] = color & 0xff;
	e->a.data[wow + 1] = color >> 8 & 0xff;
	e->a.data[wow + 2] = color >> 16 & 0xff;
}
