/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/30 14:48:54 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 12:59:13 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	axis_1(double x1, double y1, double error, t_env *e)
{
	e->coeff = ((double)e->y2 - (double)y1) / ((double)e->x2 - (double)x1);
	if (e->x > e->x2)
		return (axis_1_s(e));
	while (e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x / e->mm.size_x)])
	{
		if (e->opt.opacity)
			print_pix(e);
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
	|| (int)e->x / (int)e->mm.size_x >= e->p.x_len || (int)e->y < 0
	|| (int)e->x < 0)
			return ;
		if (e->p.map[(int)(e->y / e->mm.size_y)]\
				[(int)(e->x / e->mm.size_x)] > '0')
		{
			e->prev_angle = AXIS_ONE_S;
			return (calculate_dist(e));
		}
		e->prev_angle = AXIS_ONE;
		error += e->coeff;
		e->y += (error >= 0.5) ? 0.1 : 0;
		error -= (error >= 0.5) ? 1 : 0;
		e->x += 0.1;
	}
}

static void	axis_3(double x1, double y1, double error, t_env *e)
{
	e->coeff = ((double)e->x2 - (double)x1) / ((double)e->y2 - (double)y1);
	if (e->prev_angle != AXIS_FOUR && (e->prev_angle == AXIS_FOUR_S\
	|| e->prev_angle == AXIS_TWO || e->prev_angle == AXIS_THREE_S))
	{
		e->prev_angle = AXIS_THREE_S;
		return (axis_3_s(e));
	}
	e->prev_angle = AXIS_THREE;
	while (e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x / e->mm.size_x)])
	{
		if (e->opt.opacity)
			print_pix(e);
		if ((int)e->y < 0 || (int)e->x < 0 || (int)e->y / (int)e->mm.size_y
			>= e->p.y_len || (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		if (e->p.map[(int)(e->y / e->mm.size_y)]\
				[(int)(e->x / e->mm.size_x)] > '0')
			return (calculate_dist(e));
		error -= e->coeff;
		e->x += (error >= 0.5) ? 0.1 : 0;
		error -= (error >= 0.5) ? 1 : 0;
		e->y -= 0.1;
	}
}

static void	axis_2(double x1, double y1, double error, t_env *e)
{
	e->coeff = ((double)e->x2 - (double)x1) / ((double)e->y2 - (double)y1);
	if (e->prev_angle == AXIS_FOUR)
		return (axis_3(x1, y1, error, e));
	if (e->prev_angle == AXIS_TWO_S || e->prev_angle == AXIS_THREE || (e->x
	> e->x2 == 1 && e->prev_angle != AXIS_TWO && e->prev_angle != AXIS_ONE_S))
	{
		e->prev_angle = AXIS_TWO_S;
		return (axis_2_s(e));
	}
	e->prev_angle = AXIS_TWO;
	while (e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x / e->mm.size_x)])
	{
		if (e->opt.opacity)
			print_pix(e);
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len || (int)e->x /\
	(int)e->mm.size_x >= e->p.x_len || e->y >= (int)e->mm.size_y * e->p.y_len
	|| e->x >= (int)e->mm.size_x * e->p.x_len || (int)e->y < 0 || e->x < 0
	|| e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x / e->mm.size_x)] > '0')
			return (calculate_dist(e));
		error += e->coeff;
		e->x += (error >= 0.5) ? 0.1 : 0;
		error -= (error >= 0.5) ? 1 : 0;
		e->y += 0.1;
	}
}

static void	axis_4(double x1, double y1, double error, t_env *e)
{
	e->coeff = ((double)e->y2 - (double)y1) / ((double)e->x2 - (double)x1);
	if (e->x > e->x2)
	{
		e->prev_angle = AXIS_FOUR_S;
		return (axis_4_s(e));
	}
	while (e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x / e->mm.size_x)])
	{
		e->prev_angle = AXIS_FOUR;
		if (e->opt.opacity)
			print_pix(e);
		if ((int)e->y < 0 || (int)e->x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
		|| (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		error -= e->coeff;
		if (e->p.map[(int)((e->y / e->mm.size_y))][(int)((e->x
			/ e->mm.size_x))] > '0')
			return (calculate_dist(e));
		e->y -= (error >= 0.5) ? 0.1 : 0;
		error -= (error >= 0.5) ? 1 : 0;
		e->x += 0.1;
	}
}

void		bresenham(double x1, double y1, t_env *e)
{
	double	dx;
	double	dy;
	double	error;

	error = 0;
	e->x = x1;
	e->y = y1;
	e->x1 = x1;
	e->y1 = y1;
	dy = (double)e->y2 - y1;
	dx = (double)e->x2 - x1;
	if ((dy == 0 || dx == 0))
		zeros_stick_r(x1, y1, e);
	else if (((dx >= dy && dx > 0 && dy > 0)
		|| (dx < 0 && dy < 0 && fabs(dy) <= fabs(dx))))
		axis_1(x1, y1, error, e);
	else if ((dy >= dx && dx > 0 && dy > 0)
		|| (dx < 0 && dy <= 0 && dy <= dx))
		axis_2(x1, y1, error, e);
	else if ((dy > 0 && dx < 0 && dy >= fabs(dx))
		|| (dy < 0 && dx > 0 && fabs(dy) >= dx))
		axis_3(x1, y1, error, e);
	else if ((dx > 0 && dy < 0 && dx >= fabs(dy))
			|| (dx < 0 && dy > 0 && fabs(dx) >= dy))
		axis_4(x1, y1, error, e);
}
