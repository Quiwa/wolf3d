/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fire_ray.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 22:08:07 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:08:09 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	add_cadre(int i, t_env *e)
{
	int x;

	x = -1;
	while (++x < CADRE)
	{
		print_pix_wall(i, x, 0x000000, e);
	}
	x = WIN_Y - CADRE;
	while (++x < WIN_Y)
	{
		print_pix_wall(i, x, 0x000000, e);
	}
}

void		*fire_ray(void *arg)
{
	int		idx_min;
	int		idx_max;
	float	alpha;
	float	beta;
	float	d;
	t_env *e;

	e = (t_env *)arg;
	idx_min = e->thread_idx * WIN_X / 8 - 1;
	idx_max = idx_min + WIN_X / 8;
	while (++idx_min <= idx_max)
	{
		d = idx_min < WIN_X / 2 ? (float)(WIN_X / 2 - idx_min)
			: (float)(idx_min - WIN_X / 2);
		alpha = atan(1.15470053838 * d / WIN_X);
		e->texture[e->text].percent_x = calculate_percent_x(e);
		determine_orientation(e->x1, e->y1, e);
		bresenham(e->x1, e->y1, e);
		e->opt.opacity = 1;
		e->xr.dist *= cos(alpha);
		beta = (idx_min
		< WIN_X / 2) ? 0.523599 - alpha : alpha + 0.523599;
		e->x2 = e->ad.x + (e->xr.dist * (float)5) * cos(e->angle_min + beta);
		e->y2 = e->ad.y + (e->xr.dist * (float)5) * sin(e->angle_min + beta);
		print_walls(idx_min, e);
		e->mm.size_y = e->mm.size_x;
		add_cadre(idx_min, e);
		e->opt.opacity = e->opt.opacity_tmp;
	}
	return (arg);
}
