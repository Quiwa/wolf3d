/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x_ray.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 10:52:43 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 21:56:51 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	design_cross(t_env *e)
{
	double	tmp_x1;
	double	tmp_y1;
	double	tmp_x2;
	double	tmp_y2;

	e->ad.tmp_x = e->ad.x;
	e->ad.tmp_y = e->ad.y;
	e->ad.x = WIN_X / 2;
	e->ad.y = WIN_Y / 2;
	e->ad.x2 = e->ad.x + ((double)e->mm.size_x / 3) * 2;
	e->ad.y2 = e->ad.y + ((double)e->mm.size_y / 3) * 2;
	e->ad.x1 = e->ad.x2 - ((double)e->mm.size_x / 3) * 2;
	e->ad.y1 = e->ad.y2 - ((double)e->mm.size_y / 3) * 2;
	tmp_x1 = e->ad.x1;
	tmp_y1 = e->ad.y1;
	tmp_x2 = e->ad.x2;
	tmp_y2 = e->ad.y2;
	bresenham_ad(e->ad.x1, e->ad.y1, e);
	e->ad.x2 = tmp_x2;
	e->ad.y2 = tmp_y1;
	e->ad.x1 = tmp_x1;
	e->ad.y1 = tmp_y2;
	bresenham_ad(e->ad.x1, e->ad.y1, e);
	e->ad.x = e->ad.tmp_x;
	e->ad.y = e->ad.tmp_y;
}

static void	link_angles(t_env *e)
{
	e->ad.tmp_x = e->x1;
	e->ad.tmp_y = e->y1;
	e->opt.opacity_tmp = e->opt.opacity;
	e->texture[e->text].x = -1;
	e->thread_idx = -1;
	while (++e->thread_idx < 8)
	{
		pthread_create(&e->thread[e->thread_idx], NULL, fire_ray, e);
		pthread_join(e->thread[e->thread_idx], NULL);
	}
	design_cross(e);
}

static void	draw_view(int x, int y, t_env *e)
{
	float	percent;
	int		i;

	i = -1;
	e->dist = 50;
	e->x2 = x;
	e->y2 = y;
	e->x2 = e->ad.x + e->dist * cos(e->angle);
	e->y2 = e->ad.y + e->dist * sin(e->angle);
	percent = calculate_percent_x(e);
	e->color = 0xff0000;
	bresenham(e->ad.x, e->ad.y, e);
	e->angle_min = e->angle - 0.523599;
	link_angles(e);
}

int			x_ray(int x, int y, t_env *e)
{
	float	roat_x;
	float	roat_y;

	e->mouse_x = x;
	e->mouse_y = y;
	if (e->menu_vue)
		return (0);
	mini_adventurer(e);
	e->mm.x = e->ad.x * (double)e->mm.size_x;
	e->mm.y = e->ad.y * (double)e->mm.size_y;
	e->prev_angle_YO = e->angle;
	e->angle = 1.14;
	roat_x = ((double)x * 100 + e->x_sensibility * 10) / 180 / 100;
	roat_y = (y % WIN_Y < 0) ? (y % WIN_Y) * -1 : y % WIN_Y;
	e->rotation = (float)(x % WIN_X) / (float)WIN_X;
	e->angle_y = (float)y / WIN_Y;
	e->angle *= (roat_x * (1 + e->x_sensibility));
	e->ad.angle = e->angle;
	draw_view(x, y, e);
	if (e->opt.mini_map)
		mini_adventurer(e);
	mlx_put_image_to_window(e->a.mlx, e->a.win, e->a.img, 0, 0);
	if (e->help)
		draw_help(e);
	return (1);
}
