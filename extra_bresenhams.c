/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra_bresenhams.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 15:58:58 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 15:59:25 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	zeros_stick_r(double x1, double y1, t_env *e)
{
	if (e->x > e->x2 || e->y > e->y2)
	{
		e->x = e->x2;
		e->x2 = x1;
		e->y = e->y2;
		e->y2 = y1;
	}
	while (e->x < e->x2 || e->y < e->y2)
	{
		if (e->x < e->x2)
			e->x += 0.1;
		else
			e->y += 0.1;
		if (e->opt.opacity)
			print_pix(e);
		if ((int)e->y < 0 || (int)e->x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y > e->p.y_len ||\
				(int)e->x / (int)e->mm.size_x > e->p.x_len)
			return ;
		if (e->p.map[(int)(e->y / e->mm.size_y)][(int)(e->x /\
					e->mm.size_x)] > '0')
			return (calculate_dist(e));
	}
}

int		zeros_stick_mm(int x1, int y1, t_env *env)
{
	if (env->mm.x > env->mm.x2)
	{
		env->mm.x = env->mm.x2;
		env->mm.x2 = x1;
	}
	if (env->mm.y > env->mm.y2)
	{
		env->mm.y = env->mm.y2;
		env->mm.y2 = y1;
	}
	while (env->mm.x != env->mm.x2 || env->mm.y != env->mm.y2)
	{
		if (env->mm.x != env->mm.x2)
			++env->mm.x;
		else
			++env->mm.y;
		print_pix_mm(env);
	}
	return (1);
}
