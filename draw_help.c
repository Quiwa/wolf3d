/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_drawtext.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 11:50:10 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:41:47 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		ft_drawnbr(t_env *e, char *str, int x, int y)
{
	int		color;

	color = 0xFFFFFF;
	mlx_string_put(e->a.mlx, e->a.win, x, y, color, str);
	free(str);
}

static void	write_message(char *message, int height, t_env *e, int color)
{
	mlx_string_put(e->a.mlx, e->a.win, WIN_X - 330, height, color, message);
}

void		draw_help(t_env *e)
{
	int		color;
	int		height;

	color = 0xFF0000;
	height = WIN_X - 200;
	mlx_string_put(e->a.mlx, e->a.win, WIN_X - 280, height, color,
			"-- Menu d'aide --");
	height += 20;
	write_message("Main menu: delete", height, e, color);
	height += 20;
	write_message("Avancer: WASD / fleches", height, e, color);
	height += 20;
	write_message("Afficher la minimap: M", height, e, color);
	height += 20;
	write_message("Gerer les textures: T", height, e, color);
	height += 20;
	write_message("Menu d'aide: H", height, e, color);
	height += 20;
	write_message("Courir/marcher: shift", height, e, color);
}
