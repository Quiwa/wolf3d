/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   formulas.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 09:54:56 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:02:56 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static double	square(double nb)
{
	return (nb * nb);
}

void			determine_orientation_2(double x1, double y1, t_env *e)
{
	if (y1 > e->y2 && e->ort.orientation == HORIZONTAL)
		e->text = 1;
	else if (y1 < e->y2 && e->ort.orientation == HORIZONTAL)
		e->text = 0;
	else if (x1 > e->x2 && e->ort.orientation == VERTICAL)
		e->text = 2;
	else if (x1 < e->x2 && e->ort.orientation == VERTICAL)
		e->text = 3;
	else
	{
		e->text = 9;
		e->color = 0x606060;
	}
	e->ort.prev_x = e->x;
	e->ort.prev_y = e->y;
}

void			determine_orientation(double x1, double y1, t_env *e)
{
	if (e->ort.prev_x != e->x && e->ort.prev_y == e->y)
	{
		e->ort.orientation = HORIZONTAL;
		e->color = 0xffaa33;
	}
	else if (e->ort.prev_y != e->y && e->ort.prev_x == e->x)
	{
		e->ort.orientation = VERTICAL;
		e->color = 0xbb7722;
	}
	else if (e->color == 0xff0000 && e->ort.orientation == VERTICAL)
	{
		e->ort.orientation = HORIZONTAL;
		e->color = 0xffaa33;
	}
	else if (e->color == 0xff0000)
	{
		e->ort.orientation = VERTICAL;
		e->color = 0xbb7722;
	}
	determine_orientation_2(x1, y1, e);
	if (e->p.map[(int)(e->y / e->mm.size_x)][(int)(e->x / e->mm.size_x)] == '4')
		e->text = 4;
}

double			calculate_percent_x(t_env *e)
{
	double test;

	if (e->ort.orientation == HORIZONTAL)
	{
		test = e->mm.size_x;
		while (e->x > test)
			test += e->mm.size_x;
		if (e->mm.size_x < test)
			test -= e->mm.size_x;
		return ((double)(e->x - test) / ((double)e->mm.size_x));
	}
	else
	{
		test = e->mm.size_y;
		while (e->y > test)
			test += e->mm.size_y;
		if (e->mm.size_y < test)
			test -= e->mm.size_y;
		return ((double)(e->y - test) / ((double)e->mm.size_y));
	}
}

void			calculate_dist(t_env *e)
{
	e->xr.dist = sqrt(square(e->x - e->x1) + square(e->y - e->y1)) / 2;
	return ;
}
