/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham_mm.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/03 11:22:16 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 16:45:00 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	axis_1(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->mm.x = (env->mm.x2 > x1) ? x1 : env->mm.x2;
	env->mm.y = (env->mm.y2 > y1) ? y1 : env->mm.y2;
	dy = (double)env->mm.y2 - (double)y1;
	dx = (double)env->mm.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	env->mm.x2 = (env->mm.x2 > x1) ? env->mm.x2 : x1;
	env->mm.y2 = (env->mm.y2 > y1) ? y1 : env->mm.y2;
	while (coeff >= 0 && coeff <= 1 && env->mm.x
			!= env->mm.x2 && env->mm.x2 > env->mm.x && dx != 0)
	{
		print_pix_mm(env);
		error += coeff;
		if (error >= 0.5)
		{
			++env->mm.y;
			error = error - 1;
		}
		++env->mm.x;
	}
}

static void	axis_2(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->mm.x = (env->mm.x2 > x1) ? x1 : env->mm.x2;
	env->mm.y = (env->mm.y2 > y1) ? y1 : env->mm.y2;
	dy = (double)env->mm.y2 - (double)y1;
	dx = (double)env->mm.x2 - (double)x1;
	coeff = dx / dy;
	error = 0;
	env->mm.x2 = (env->mm.x2 > env->mm.x) ? env->mm.x2 : x1;
	env->mm.y2 = (env->mm.y2 > env->mm.y) ? env->mm.y2 : y1;
	while (coeff > 0 && env->mm.y2 > env->mm.y && env->mm.y != env->mm.y2)
	{
		print_pix_mm(env);
		error += coeff;
		if (error >= 0.5)
		{
			++env->mm.x;
			error = error - 1;
		}
		++env->mm.y;
	}
}

static void	axis_3(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->mm.x = (env->mm.x2 > x1) ? env->mm.x2 : x1;
	env->mm.y = (env->mm.y2 > y1) ? y1 : env->mm.y2;
	dy = (double)env->mm.y2 - (double)y1;
	dx = (double)env->mm.x2 - (double)x1;
	env->mm.x2 = (env->mm.x2 < x1) ? x1 : env->mm.x2;
	env->mm.y2 = (env->mm.y2 > y1) ? env->mm.y2 : y1;
	coeff = dx / dy;
	error = 0;
	while (coeff <= 0 && env->mm.y2 > env->mm.y && env->mm.y != env->mm.y2)
	{
		print_pix_mm(env);
		error -= coeff;
		if (error >= 0.5)
		{
			--env->mm.x;
			error = error - 1;
		}
		++env->mm.y;
	}
}

static void	axis_4(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->mm.x = (env->mm.x2 > x1) ? x1 : env->mm.x2;
	env->mm.y = (env->mm.y2 < y1) ? y1 : env->mm.y2;
	dy = (double)env->mm.y2 - (double)y1;
	dx = (double)env->mm.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	env->mm.x2 = (env->mm.x2 > env->mm.x) ? env->mm.x2 : x1;
	env->mm.y2 = (env->mm.y2 > env->mm.x) ? env->mm.y2 : y1;
	while (coeff < 0 && env->mm.x != env->mm.x2 && env->mm.x2 > env->mm.x)
	{
		print_pix_mm(env);
		error -= coeff;
		if (error >= 0.5)
		{
			--env->mm.y;
			error = error - 1;
		}
		++env->mm.x;
	}
}

int			bresenham_mm(double x1, double y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->mm.x = x1;
	env->mm.y = y1;
	dy = (double)env->mm.y2 - (double)y1;
	dx = (double)env->mm.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	if (dy == 0.0 || dx == 0.0)
		return (zeros_stick_mm(x1, y1, env));
	if (((dx >= dy && dx > 0 && dy > 0) ||
				(dx < 0 && dy < 0 && fabs(dy) <= fabs(dx))))
		axis_1(x1, y1, env);
	else if ((dy > dx && dx > 0 && dy > 0) ||
			(dx < 0 && dy < 0 && dy < dx))
		axis_2(x1, y1, env);
	else if ((dy > 0 && dx < 0 && dy >= fabs(dx))
			|| (dy < 0 && dx > 0 && fabs(dy) > dx))
		axis_3(x1, y1, env);
	else
		axis_4(x1, y1, env);
	return (0);
}
