/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 16:23:47 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 21:20:31 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	to_map(char *line, t_env *e)
{
	char	**tmp;
	int		idx;

	idx = -1;
	tmp = NULL;
	if (!(tmp = ft_strsplit(line, ' ')))
		exit_m(E_MALLOC);
	if (!(e->p.map[++e->p.idx_l] = ft_strnew(e->p.x_len + 3)))
		exit_m(E_MALLOC);
	e->p.map[e->p.idx_l][0] = WALL_R;
	while (tmp[++idx])
	{
		if (!(tmp[idx][0] >= '0' && tmp[idx][0] <= '9'))
			exit_m(E_MAP);
		e->p.map[e->p.idx_l][idx + 1] = tmp[idx][0];
	}
	e->p.map[e->p.idx_l][++idx] = WALL_R;
	e->p.map[e->p.idx_l][++idx] = '\0';
	idx = -1;
	while (tmp[++idx])
		free(tmp[idx]);
	free(tmp);
}

static void	parser_column(char **av, t_env *e)
{
	char	*line;
	int		fd;
	int		idx;

	idx = -1;
	if (!(fd = open(av[1], O_RDONLY)))
		exit_m(E_OPEN);
	while (get_next_line(fd, &line) > 0)
	{
		if (ft_wordcount(line, ' ') != e->p.x_len)
		{
			free(line);
			exit_m(E_MAP);
		}
		to_map(line, e);
		free(line);
	}
	if (!(e->p.map[++e->p.idx_l] = ft_strnew(e->p.x_len + 2)))
		exit_m(E_MALLOC);
	while (++idx < e->p.x_len + 2)
		e->p.map[e->p.idx_l][idx] = WALL_R;
	if (!(e->p.map[++e->p.idx_l] = malloc(sizeof(char))))
		exit_m(E_MALLOC);
	e->p.map[e->p.idx_l][0] = '\0';
}

void		adapt_map(t_env *e)
{
	int	i;
	int	j;

	i = 0;
	while (++i < e->p.y_len)
	{
		j = 0;
		while (++j < e->p.x_len)
		{
			if (i - 1 >= 0 && j - 1 >= 0 && i + 1 < e->p.y_len
					&& j + 1 < e->p.x_len)
			{
				if (e->p.map[i + 1][j] && e->p.map[i][j + 1])
				{
					if (e->p.map[i + 1][j] != '0' && e->p.map[i][j + 1] != '0')
						e->p.map[i][j] = WALL_R;
				}
			}
		}
	}
}

void		parser(char **av, t_env *e)
{
	int		fd;
	char	*line;

	if ((fd = open(av[1], O_RDONLY)) < 0)
		exit_m(E_OPEN);
	while (get_next_line(fd, &line) > 0)
	{
		e->p.y_len++;
		e->p.x_len = (e->p.y_len == 1) ? ft_wordcount(line, ' ') : e->p.x_len;
		free(line);
	}
	close(fd);
	if (!(e->p.map = malloc(sizeof(char *) * (e->p.y_len + 3))))
		exit_m(E_MALLOC);
	if (!(e->p.map[0] = ft_strnew(e->p.x_len + 2)))
		exit_m(E_MALLOC);
	while (++e->i < e->p.x_len + 2)
		e->p.map[0][e->i] = WALL_R;
	parser_column(av, e);
	if (e->p.y_len == 0)
		exit_m(E_EMPTY_MAP);
	e->p.y_len += 2;
	e->p.x_len += 2;
	adapt_map(e);
}
