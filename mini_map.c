/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 16:28:17 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:38:35 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	paint_wall(int y, int x, t_env *e)
{
	float y1;

	e->mm.color = (e->p.map[y][x] != '4') ? 0x233142 : 0xa52a2a;
	e->mm.start_x = x * e->mm.size_x - 1;
	y1 = y * e->mm.size_y - 1;
	e->mm.start_y = y1 + 1;
	e->mm.end_x = (x + 1) * e->mm.size_x;
	e->mm.end_y = (y + 1) * e->mm.size_y;
	while (++y1 <= e->mm.end_y)
	{
		e->mm.x = e->mm.start_x;
		e->mm.y = y1;
		e->mm.y2 = y1;
		e->mm.x2 = e->mm.end_x;
		bresenham_mm(e->mm.x, e->mm.y, e);
	}
}

static void	fill_walls(t_env *e)
{
	int	x;
	int	y;

	y = -1;
	while (++y < e->p.y_len)
	{
		x = -1;
		while (++x < e->p.x_len && e->p.map[y][x])
			if (e->p.map[y][x] > '0')
				paint_wall(y, x, e);
	}
}

static void	draw_grid(t_env *e)
{
	e->mm.idx_c = -1.0;
	while (++e->mm.idx_c < e->p.x_len)
	{
		e->mm.x2 = (e->mm.idx_c + 1) * e->mm.size_x;
		e->mm.y2 = (e->mm.idx_l) * e->mm.size_y;
		bresenham_mm(e->mm.idx_c * e->mm.size_x, e->mm.y2, e);
		if (e->mm.idx_l == e->p.y_len)
			continue;
		e->mm.x2 = (e->mm.idx_c) * e->mm.size_x;
		e->mm.y2 = (e->mm.idx_l + 1) * e->mm.size_y;
		bresenham_mm(e->mm.x2, (e->mm.idx_l) * e->mm.size_y, e);
	}
	if (e->mm.idx_l == e->p.y_len)
		return ;
	e->mm.x2 = (e->mm.idx_c) * e->mm.size_x;
	e->mm.y2 = (e->mm.idx_l + 1) * e->mm.size_y;
	bresenham_mm(e->mm.x2, (e->mm.idx_l) * e->mm.size_y, e);
}

int			mini_map(t_env *e)
{
	//e->mm.color = 0xffffff;
	e->mm.color = 0xd6cecc;
	e->mm.idx_l = -1.0;
	while (++e->mm.idx_l <= e->p.y_len)
		draw_grid(e);
	fill_walls(e);
	return (0);
}
