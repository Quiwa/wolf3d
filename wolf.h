/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 16:23:09 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:07:20 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

//# include "minilibx_macos/mlx.h"
# include "minilibx_macos/mlx2.h"
# include "libft/srcs/libft.h"
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <math.h>
# include <pthread.h>

/*
** Formulas
*/
# define DEG_TO_RAD(x) (x * 2 * M_PI) / 360

/*
** Parametrables
*/
# define MINI_MAP 46

/*
** Window dimensions
*/

# define WIN_X 1000
# define WIN_Y 1000
# define CADRE 55

/*
** Wall orientation
*/
# define HORIZONTAL 1
# define VERTICAL 2

/*
** MENU values
*/
# define START 0
# define SETTINGS 1
# define Y_LEFT 3
# define Y_MID 4
# define Y_RIGHT 5
# define X_LEFT 6
# define X_MID 7
# define X_RIGHT 8
# define BACK 9
# define QUIT 2
# define MENU 10

/*
** Keys
*/
# define EXIT 53
# define THREADS 8
# define RIGHT 124
# define DOWN 125
# define LEFT 123
# define UP 126
# define W 13
# define S 1
# define A 0
# define D 2
# define TEXTURED 17
# define SHIFT 257
# define HELP 4
# define ENTER 36
# define SELECT 51

/*
** Exit values
*/
# define E_PARAM 1
# define E_MAP 2
# define E_EMPTY_MAP 6
# define E_OPEN 3
# define E_MALLOC 4
# define E_MLX 5

/*
** Walls
*/
# define WALL_R '1'
# define EMPTY '0'
# define TEXTURES 5

/*
** Mathematical values
*/
# define PI 3.14159265359
# define TAN 0.577350269189626
# define DISTORTION 1.0472 / WIN_X

/*
** BRESENHAM AXIS
*/
# define AXIS_ONE 1
# define AXIS_TWO 2
# define AXIS_THREE 3
# define AXIS_FOUR 4
# define AXIS_ONE_S 5
# define AXIS_TWO_S 6
# define AXIS_THREE_S 7
# define AXIS_FOUR_S 8
# define AXIS_UP 9
# define AXIS_LEFT 10
# define AXIS_RIGHT 11
# define AXIS_DOWN 12

typedef struct		s_aff
{
	void			*mlx;
	void			*win;
	void			*img;
	unsigned char	*data;
	int				endian;
	int				bpp;
	int				size_line;
}					t_aff;

typedef struct		s_parse
{
	int				y_len;
	int				x_len;
	int				idx_l;
	char			**map;
}					t_parse;

typedef struct		s_adventurer
{
	double			tmp_x;
	double			tmp_y;
	double			x;
	double			y;
	double			x1;
	double			y1;
	double			x2;
	double			y2;
	int				color;
	int 			bres_axis;
	double			error;
	int 			angle_changed;
	double 		prev_angle;
	double			angle;
	double			coeff;
	double			ec_x;
	double			ec_y;
}					t_adventurer;

typedef struct		s_mmap
{
	double				size_x;
	double				size_y;
	double				idx_c;
	double				idx_l;
	double				start_x;
	double				end_x;
	double				start_y;
	double				end_y;
	double				x;
	double				y;
	double				old_x;
	double				old_y;
	double				x2;
	double				y2;
	int				color;
	int 			calc_ec;
}					t_mmap;

typedef struct		s_xray
{
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	int       dist_changed;
	double			dist_n;
	double			dist;
}					t_xray;

typedef struct		s_texture
{
	unsigned char			*data;
	void    *addr;
	int			width;
	int			height;
	int			x;
	int			y;
	int     bpp;
	int     endian;
	int     size_line;
	double	percent_y;
	double	percent_x;
}			t_texture;

typedef struct		s_menu
{
	unsigned char			*data;
	void    *addr;
	int			width;
	int			height;
	int     bpp;
	int     size_line;
	int     endian;
}			t_menu;

typedef struct		s_moov
{
		double	percent_x;
		double	percent_y;
		double	percent;
}			t_moov;

typedef struct    s_orientation
{
	double       prev_x;
	double       prev_y;
	int 				orientation;
}     						t_or;

typedef struct 		s_options
{
	int					run;
	int					mini_map;
	double				opacity;
	double				opacity_tmp;
} 								t_opt;
typedef struct		s_env
{
	t_aff			a;
	t_parse			p;
	t_mmap			mm;
	t_xray			xr;
	t_adventurer	ad;
	t_texture		texture[TEXTURES];
	t_moov			moov;
	t_or        ort;
	t_opt 			opt;
	t_menu 			menu[MENU];
	int				color;
	int 			textured_walls;
	double 			speed;
	double			x;
	double			y;
	double			x1;
	double			y1;
	double			x2;
	double			y2;
	double			dx;
	double			dy;
	double			prev_angle_YO;
	double			angle;
	double			angle_y;
	double			rotation;
	double 		prev_angle;
	float			cosinus;
	float			lost;
	int				dist;
	int				mouse_x;
	int				mouse_y;
	int				help;
	double			coeff;
	double			moved;
	int 			text;
	int 			menu_vue;
	int 			menu_idx;
	int 			x_sensibility;
	int 			y_sensibility;
	int 			y_print;
	int				i;
	float			angle_min;
	pthread_t		thread[8];
	int				thread_idx;
}					t_env;

int move_up(t_env *e);
int move_down(t_env *e);
int move_left(t_env *e);
int move_right(t_env *e);
void	parser(char **av, t_env *e);
void	exit_m(int error);
int		key_event(int key, t_env *e);
int		x_ray(int x, int y, t_env *e);
int		destroy_win(void);
void	free_parse(t_env *e);
int	mini_map(t_env *e);
int	mini_adventurer(t_env *e);
void	print_pix(t_env *e);
void	print_pix_mm(t_env *e);
void	print_pix_ad(t_env *e);
void	print_pix_wall(int x, int y, int color, t_env *e);
void	bresenham(double x1, double y1, t_env *e);
void	axis_1_s(t_env *e);
void	axis_2_s(t_env *e);
void	axis_3_s(t_env *e);
void	axis_4_s(t_env *e);
int	bresenham_mm(double x1, double y1, t_env *e);
int	bresenham_ad(double x1, double y1, t_env *e);
void	calculate_dist(t_env *e);
void		field_of_vision(t_env *e);
void		print_walls(int x, t_env *e);
double		calculate_percent_x(t_env *e);
void 	determine_orientation(double x1, double y1, t_env *e);
void 	ft_drawtext(t_env *e);
void 	menu(int key, t_env *e);
void	init_menu(t_env *e);
void  zeros_stick_r(double x1, double y1, t_env *e);
int 	zeros_stick_mm(int x1, int y1, t_env *e);
void	*fire_ray(void *arg);
void	draw_help(t_env *e);

#endif
