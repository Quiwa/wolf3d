/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_up.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 14:05:14 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 21:57:22 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define TEST 1
#include "wolf.h"

int	move_up(t_env *e)
{
	int new_x;
	int new_y;

	e->moov.percent_x = 0.0;
	e->moov.percent_y = 0.0;
	e->moov.percent = (int)(e->ad.angle / 6.28);
	e->moov.percent = (e->ad.angle - (6.28 * e->moov.percent)) / 6.28;
	e->moov.percent_x = cos(e->ad.angle);
	e->moov.percent_y = sin(e->ad.angle);
	new_x = (e->ad.x + (e->moov.percent_x * (double)e->speed)) / e->mm.size_x;
	new_y = (e->ad.y + (e->moov.percent_y * (double)e->speed)) / e->mm.size_x;
	if (e->p.map[new_y][new_x] != '1')
	{
		e->ad.x += (e->moov.percent_x * (double)e->speed);
		e->ad.y += (e->moov.percent_y * (double)e->speed);
	}
	else if (e->p.map[(int)((e->ad.y) / e->mm.size_x)][new_x] != '1')
		e->ad.x += (e->moov.percent_x * (double)e->speed);
	else if (e->p.map[new_y][(int)((e->ad.x) / e->mm.size_x)] != '1')
		e->ad.y += (e->moov.percent_y * (double)e->speed);
	return (1);
}

int	move_down(t_env *e)
{
	int	new_x;
	int	new_y;

	e->moov.percent_x = 0.0;
	e->moov.percent_y = 0.0;
	e->moov.percent = (int)(e->ad.angle / 6.28);
	e->moov.percent = (e->ad.angle - (6.28 * e->moov.percent)) / 6.28;
	e->moov.percent_x = cos(e->ad.angle);
	e->moov.percent_y = sin(e->ad.angle);
	new_x = (e->ad.x - (e->moov.percent_x * (double)e->speed)) / e->mm.size_x;
	new_y = (e->ad.y - (e->moov.percent_y * (double)e->speed)) / e->mm.size_x;
	if (e->p.map[new_y][new_x] != '1')
	{
		e->ad.x -= (e->moov.percent_x * (double)e->speed);
		e->ad.y -= (e->moov.percent_y * (double)e->speed);
	}
	else if (e->p.map[(int)((e->ad.y) / e->mm.size_x)][new_x] != '1')
		e->ad.x -= (e->moov.percent_x * (double)e->speed);
	else if (e->p.map[new_y][(int)((e->ad.x) / e->mm.size_x)] != '1')
		e->ad.y -= (e->moov.percent_y * (double)e->speed);
	return (1);
}

int	move_left(t_env *e)
{
	int	new_x;
	int	new_y;

	e->moov.percent_x = 0.0;
	e->moov.percent_y = 0.0;
	e->moov.percent = ((e->ad.angle) / 6.28);
	e->moov.percent = (e->ad.angle - (6.28 * e->moov.percent)) / 6.28;
	e->moov.percent_x = cos(e->ad.angle + 1.25);
	e->moov.percent_y = sin(e->ad.angle + 1.25);
	new_x = (e->ad.x - (e->moov.percent_x * (double)e->speed)) / e->mm.size_x;
	new_y = (e->ad.y - (e->moov.percent_y * (double)e->speed)) / e->mm.size_x;
	if (e->p.map[new_y][new_x] != '1')
	{
		e->ad.x -= (e->moov.percent_x * (double)e->speed);
		e->ad.y -= (e->moov.percent_y * (double)e->speed);
	}
	else if (e->p.map[(int)((e->ad.y) / e->mm.size_x)][new_x] != '1')
		e->ad.x -= (e->moov.percent_x * (double)e->speed);
	else if (e->p.map[new_y][(int)((e->ad.x) / e->mm.size_x)] != '1')
		e->ad.y -= (e->moov.percent_y * (double)e->speed);
	return (1);
}

int	move_right(t_env *e)
{
	int	new_x;
	int	new_y;

	e->moov.percent_x = 0.0;
	e->moov.percent_y = 0.0;
	e->moov.percent = ((e->ad.angle) / 6.28);
	e->moov.percent = (e->ad.angle - (6.28 * e->moov.percent)) / 6.28;
	e->moov.percent_x = cos(e->ad.angle + 1.25);
	e->moov.percent_y = sin(e->ad.angle + 1.25);
	new_x = (e->ad.x + (e->moov.percent_x * (double)e->speed)) / e->mm.size_x;
	new_y = (e->ad.y + (e->moov.percent_y * (double)e->speed)) / e->mm.size_x;
	if (e->p.map[new_y][new_x] != '1')
	{
		e->ad.x += (e->moov.percent_x * (double)e->speed);
		e->ad.y += (e->moov.percent_y * (double)e->speed);
	}
	else if (e->p.map[(int)((e->ad.y) / e->mm.size_x)][new_x] != '1')
		e->ad.x -= (e->moov.percent_x * (double)e->speed);
	else if (e->p.map[new_y][(int)((e->ad.x) / e->mm.size_x)] != '1')
		e->ad.y -= (e->moov.percent_y * (double)e->speed);
	return (1);
}
