# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/02 16:58:52 by jlehideu          #+#    #+#              #
#    Updated: 2018/12/12 19:55:21 by jlehideu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

SRC = main.c\
	  parser.c\
	  exit_messages.c\
	  key_event.c\
	  x_ray.c\
	  destroy_win.c\
	  mini_map.c\
	  mini_adventurer.c\
	  print_pix.c\
	  bresenham.c\
	  bresenham_s.c\
	  bresenham_mm.c\
	  bresenham_ad.c\
	  formulas.c\
	  print_3d.c \
	  move_up.c\
	  draw_help.c\
	  menu.c\
		extra_bresenhams.c\
		fire_ray.c

OBJ = $(SRC:%.c=%.o)

CC = gcc

DEB = -fsanitize=address -g3

FLAGS =  -Wall -Wextra -Werror


MLX = -lmlx -framework OpenGL -framework Appkit #-framework SDL2_mixer -F ~/Library/Frameworks

LIB = -L libft -lft

THREADS = -lpthread

all : $(NAME)

$(NAME): $(OBJ)
	make -C minilibx_macos/
	make -C libft/
ifdef DEBUG
		$(CC) -o $(NAME) $^ $(FLAGS) $(DEB) $(LIB) $(MLX)
else
		$(CC) -o $(NAME) $^ $(FLAGS) $(LIB) $(MLX)
endif

debug	:	fclean
	make DEBUG=1

%.o: %.c
ifdef DEBUG
		$(CC) -c $(FLAGS) $(DEB) $(L) $<
else
		$(CC) -c $(FLAGS) $(L) $<
endif

clean:
	rm -f $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

yo: all clean
