/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham_ad.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <adrvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 18:07:50 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 13:37:51 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static int	zeros_stick(int x1, int y1, t_env *env)
{
	if (env->ad.x > env->ad.x2)
	{
		env->ad.x = env->ad.x2;
		env->ad.x2 = x1;
	}
	if (env->ad.y > env->ad.y2)
	{
		env->ad.y = env->ad.y2;
		env->ad.y2 = y1;
	}
	while (env->ad.x != env->ad.x2 || env->ad.y != env->ad.y2)
	{
		if (env->ad.x != env->ad.x2)
			++env->ad.x;
		else
			++env->ad.y;
		print_pix_ad(env);
	}
	return (1);
}

static void	axis_1(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->ad.x = (env->ad.x2 > x1) ? x1 : env->ad.x2;
	env->ad.y = (env->ad.y2 > y1) ? y1 : env->ad.y2;
	dy = (double)env->ad.y2 - (double)y1;
	dx = (double)env->ad.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	env->ad.x2 = (env->ad.x2 > x1) ? env->ad.x2 : x1;
	env->ad.y2 = (env->ad.y2 > env->ad.y1) ? env->ad.y2 : env->ad.y1;
	while (coeff >= 0 && env->ad.x
			!= env->ad.x2 && env->ad.x2 > env->ad.x && dx != 0)
	{
		print_pix_ad(env);
		error += coeff;
		if (error >= 0.5)
		{
			env->ad.y += 1;
			error = error - 1;
		}
		env->ad.x += 1;
	}
}

static void	axis_2(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->ad.x = (env->ad.x2 > x1) ? x1 : env->ad.x2;
	env->ad.y = (env->ad.y2 > y1) ? y1 : env->ad.y2;
	dy = (double)env->ad.y2 - (double)y1;
	dx = (double)env->ad.x2 - (double)x1;
	coeff = dx / dy;
	error = 0;
	env->ad.x2 = (env->ad.x2 > env->ad.x) ? env->ad.x2 : x1;
	env->ad.y2 = (env->ad.y2 > env->ad.y) ? env->ad.y2 : y1;
	while (coeff > 0 && env->ad.y2 > env->ad.y && env->ad.y != env->ad.y2)
	{
		print_pix_ad(env);
		error += coeff;
		if (error >= 0.5)
		{
			env->ad.x += 1;
			error = error - 1;
		}
		env->ad.y += 1;
	}
}

static void	axis_4(int x1, int y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->ad.x = (env->ad.x2 > x1) ? x1 : env->ad.x2;
	env->ad.y = (env->ad.y2 < y1) ? y1 : env->ad.y2;
	dy = (double)env->ad.y2 - (double)y1;
	dx = (double)env->ad.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	env->ad.x2 = (env->ad.x2 > env->ad.x) ? env->ad.x2 : x1;
	env->ad.y2 = (env->ad.y2 > env->ad.x) ? env->ad.y2 : y1;
	while (coeff < 0 && env->ad.x != env->ad.x2 && env->ad.x2 > env->ad.x)
	{
		print_pix_ad(env);
		error -= coeff;
		if (error >= 0.5)
		{
			env->ad.y -= 1;
			error = error - 1;
		}
		env->ad.x += 1;
	}
}

int			bresenham_ad(double x1, double y1, t_env *env)
{
	double	error;
	double	coeff;
	double	dx;
	double	dy;

	env->ad.x = x1;
	env->ad.y = y1;
	dy = (double)env->ad.y2 - (double)y1;
	dx = (double)env->ad.x2 - (double)x1;
	coeff = dy / dx;
	error = 0;
	if (dy == 0.0 || dx == 0.0)
		return (zeros_stick(x1, y1, env));
	if (((dx >= dy && dx > 0 && dy > 0) ||
				(dx < 0 && dy < 0 && fabs(dy) <= fabs(dx))))
		axis_1(x1, y1, env);
	else if ((dy > dx && dx > 0 && dy > 0) ||
			(dx < 0 && dy < 0 && dy < dx))
		axis_2(x1, y1, env);
	else if ((dy > 0 && dx < 0 && dy >= fabs(dx))
			|| (dy < 0 && dx > 0 && fabs(dy) > dx))
		axis_4(x1, y1, env);
	else
		axis_4(x1, y1, env);
	return (0);
}
