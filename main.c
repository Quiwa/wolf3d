/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 16:19:59 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 21:37:04 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	find_empty_spot(t_env *e)
{
	int i;
	int j;

	i = -1;
	e->mm.size_x = 14.0;
	e->mm.size_y = e->mm.size_x;
	while (++i < e->p.y_len)
	{
		j = -1;
		while (++j < e->p.x_len)
		{
			if (e->p.map[i][j] == EMPTY)
			{
				e->ad.y = i * e->mm.size_y + 1;
				e->ad.x = j * e->mm.size_x + 1;
				return ;
			}
		}
	}
	exit_m(E_MAP);
}

static void	init_textures(t_env *e)
{
	int	i;

	i = -1;
	if (!(e->texture[0].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./textures/wall.xpm", &e->texture[0].width, &e->texture[0].height)))
		return ;
	if (!(e->texture[1].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./textures/greystone.xpm", &e->texture[1].width, &e->texture[1].height)))
		return ;
	if (!(e->texture[2].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./textures/bluestone.xpm", &e->texture[2].width, &e->texture[2].height)))
		return ;
	if (!(e->texture[3].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./textures/redbrick.xpm", &e->texture[3].width, &e->texture[3].height)))
		return ;
	if (!(e->texture[4].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./textures/only_barrel.xpm", &e->texture[4].width, &e->texture[4].height)))
		return ;
	while (++i < TEXTURES)
	{
		if (!(e->texture[i].data =\
	(unsigned char *)mlx_get_data_addr(e->texture[i].addr,
	&e->texture[i].bpp, &e->texture[i].size_line, &e->texture[i].endian)))
			return ;
	}
}

static void	init_var(t_env *e)
{
	int i;

	i = -1;
	bzero(e, sizeof(*e));
	e->a.mlx = mlx_init();
	e->a.win = mlx_new_window(e->a.mlx, WIN_X, WIN_Y, "Wolf3d");
	e->a.img = mlx_new_image(e->a.mlx, WIN_X, WIN_Y);
	if (!(e->a.data = (unsigned char *)mlx_get_data_addr(e->a.img,
					&e->a.bpp, &e->a.size_line, &e->a.endian)))
		return ;
	init_textures(e);
	init_menu(e);
	e->ad.x = 30;
	e->ad.y = 30;
	e->speed = 3.0;
	e->ad.angle_changed = 1;
	e->textured_walls = 1;
	e->menu_vue = 1;
	e->y_sensibility = 1;
}

int			main(int ac, char **av)
{
	t_env	e;

	init_var(&e);
	if (ac < 2)
		exit_m(E_PARAM);
	e.i = -1;
	parser(av, &e);
	find_empty_spot(&e);
	menu(0, &e);
	mlx_hook(e.a.win, 2, (1L << 1), key_event, &e);
	mlx_hook(e.a.win, 6, (1L << 6), x_ray, &e);
	mlx_hook(e.a.win, 17, (1L << 17), destroy_win, NULL);
	mlx_loop(e.a.mlx);
	return (0);
}
