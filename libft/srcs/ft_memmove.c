/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:51:01 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/17 10:51:02 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *str1, const void *str2, size_t n)
{
	unsigned char	*ptr1;
	unsigned char	*ptr2;
	size_t			i;

	i = -1;
	ptr1 = (unsigned char *)str1;
	ptr2 = (unsigned char *)str2;
	if (ptr1 > ptr2)
		while (0 < n)
		{
			ptr1[n - 1] = ptr2[n - 1];
			--n;
		}
	else
		while (++i < n)
			ptr1[i] = ptr2[i];
	return (str1);
}
