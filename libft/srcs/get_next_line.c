/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 11:10:49 by jlehideu          #+#    #+#             */
/*   Updated: 2018/08/02 11:10:51 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

t_read		*fd_manager(int fd, t_read **cursor)
{
	t_read	*traveler;

	traveler = *cursor;
	while (traveler && traveler->fd != fd)
		traveler = traveler->next;
	if (traveler)
		return (traveler);
	if (!(traveler = (t_read *)malloc(sizeof(t_read))))
		return (NULL);
	traveler->ptr = NULL;
	traveler->fd = fd;
	traveler->next = *cursor;
	*cursor = traveler;
	return (*cursor);
}

int			copy_in_line(char **line, char *buff, char **ptr)
{
	size_t	i;
	char	*tmp;

	i = 0;
	while (buff[i] && buff[i] != '\n')
		i++;
	*line = ft_bstrjoin(*line, ft_strsub((char const*)buff, 0, (size_t)i));
	if (i < ft_strlen(buff))
		++i;
	tmp = ft_strdup(buff);
	ft_strdel(&*ptr);
	*ptr = ft_strsub((const char *)tmp, i, (ft_strlen(tmp) - i));
	if (*ptr && ft_strlen(*ptr) == 0)
		ft_strdel(ptr);
	ft_strdel(&tmp);
	return (1);
}

int			result(t_read *select, char **line)
{
	if (select->ptr && ft_strchr((const char *)select->ptr, '\n'))
	{
		copy_in_line(line, select->ptr, &select->ptr);
		return ((select->s != 0 || select->ptr || **line != '\0') ? 1 : 0);
	}
	if (select->ptr)
		if ((select->s = copy_in_line(line, select->ptr, &select->ptr)))
			if (select->ptr)
				return (1);
	return (-1);
}

static int	return_manager(t_read *cursor, t_read *select, char **line)
{
	t_read	*traveler;

	traveler = cursor;
	if (select->ptr || select->s != 0 || ft_strlen(*line) > 0)
		return (1);
	while (traveler)
	{
		if (traveler->next && traveler->next->fd == select->fd)
		{
			ft_strdel(&select->ptr);
			traveler->next = select->next;
			free(select);
		}
		traveler = traveler->next;
	}
	ft_strdel(line);
	return (0);
}

int			get_next_line(int fd, char **line)
{
	static t_read	*cursor;
	t_read			*select;
	char			*buff;

	if (fd < 0 || !line)
		return (-1);
	select = fd_manager(fd, &cursor);
	*line = ft_strdup("");
	if ((select->s = result(select, line) >= 0))
		return (select->s);
	if (!(buff = ft_strnew(BUFF_SIZE)))
		return (-1);
	while (((select->s = read(fd, buff, BUFF_SIZE)) > 0) &&
			!ft_strchr(buff, '\n'))
	{
		buff[select->s] = '\0';
		*line = ft_fstrjoin(*line, buff);
	}
	if (select->s < 0)
		return (-1);
	buff[select->s] = '\0';
	if (select->s)
		copy_in_line(line, buff, &select->ptr);
	ft_strdel(&buff);
	return (return_manager(cursor, select, line));
}
