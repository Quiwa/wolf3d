/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 11:46:02 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/16 11:46:09 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	char			*result;

	i = 0;
	i = -1;
	if (!s)
		return ((char *)s);
	result = ft_strdup(s);
	if (!result)
		return (result);
	while (s[++i])
		result[i] = (f)(i, result[i]);
	return (result);
}
