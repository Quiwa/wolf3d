/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 09:29:58 by jlehideu          #+#    #+#             */
/*   Updated: 2018/06/04 17:26:44 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strdup(const char *src)
{
	int			i;
	int			len;
	char		*dest;

	i = -1;
	if (!src)
		return (NULL);
	len = ft_strlen(src);
	dest = (char *)malloc(sizeof(char) * len + 1);
	if (!(dest))
		return (dest);
	while (++i < len)
		dest[i] = src[i];
	dest[i] = '\0';
	return (dest);
}
