/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:38:21 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/18 17:33:39 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*result;
	int		idx_r;
	int		idx_d;

	result = NULL;
	if (!s1 || !s2)
		return (0);
	idx_d = ft_strlen(s1) + ft_strlen(s2);
	if (!(result = (char*)malloc(sizeof(char) * idx_d + 2)))
		return (0);
	if (!s1)
		return (0);
	idx_r = -1;
	idx_d = -1;
	while (s1[++idx_d])
		result[++idx_r] = s1[idx_d];
	idx_d = -1;
	while (s2[++idx_d])
		result[++idx_r] = s2[idx_d];
	result[++idx_r] = '\0';
	return (result);
}
