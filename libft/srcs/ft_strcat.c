/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:48:04 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/19 11:40:15 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dst, const char *src)
{
	size_t	i;
	size_t	len_1;
	size_t	len_2;

	i = 0;
	len_1 = ft_strlen(dst);
	len_2 = ft_strlen(src);
	if (!(src[i]))
		return (dst);
	while (src[i] && i < len_2)
	{
		dst[len_1 + i] = src[i];
		i++;
	}
	dst[len_1 + i] = '\0';
	return (dst);
}
