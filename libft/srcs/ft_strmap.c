/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 11:43:56 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/16 11:43:58 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*result;

	i = -1;
	if (!s)
		return ((char *)s);
	result = ft_strdup(s);
	if (!(result))
		return (result);
	while (s[++i])
		result[i] = (f)(result[i]);
	return (result);
}
