/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:58:50 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/19 11:38:35 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char			*convert(char *result, int length, long int nb,
long int prod)
{
	if (nb == 0)
		result[++length] = '0';
	while (prod >= 1)
	{
		result[++length] = (nb / prod) + '0';
		nb = nb % prod;
		prod = prod / 10;
	}
	result[++length] = '\0';
	return (result);
}

long long int		set_product(long long int nb, int *length)
{
	long long int	product;

	product = 1;
	*length = 0;
	while (product < nb)
	{
		product = (product == 0) ? 1 : product;
		product *= 10;
		*length += 1;
	}
	if (product != nb)
		product /= 10;
	return (product);
}

char				*ft_itoa(int n)
{
	int				length;
	long long int	product;
	long long int	nb;
	char			*result;

	nb = n;
	nb = (nb > 0) ? nb : nb * -1;
	product = set_product(nb, &length);
	if ((result = (char*)malloc(sizeof(char) * length + 2)) == NULL)
		return (result);
	length = -1;
	if (n < 0 == 1)
		result[++length] = '-';
	result = convert(result, length, nb, product);
	return (result);
}
