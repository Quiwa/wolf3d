/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 11:47:38 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/19 11:41:14 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		str_len(const char *s, char c, int i)
{
	int		y;

	y = 1;
	while (s[++i] && s[i] != c)
		y++;
	return (y);
}

static int		wordcount(char const *s, char c)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (s[i])
	{
		while (s[i] && s[i] == c)
			i++;
		if (s[i] && s[i] != c)
			count++;
		while (s[i] && s[i] != c)
			i++;
	}
	return (count);
}

static int		skip_white(char const *s, char c, int i)
{
	while (s[i] == c)
		i++;
	return (i);
}

static int		next_word(char const *s, char c, int i)
{
	while (s[i] == c && s[i])
		i++;
	while (s[i] != c && s[i])
		i++;
	while (s[i] == c && s[i])
		i++;
	return (i);
}

char			**ft_strsplit(char const *s, char c)
{
	int		i_l;
	int		i;
	char	**result;

	result = NULL;
	if (!c || !s)
		return (0);
	i_l = -1;
	i = skip_white(s, c, 0);
	if (!(result = (char **)malloc(sizeof(char *) * (wordcount(s, c) + 1))))
		return (0);
	while (++i_l < wordcount(s, c))
	{
	/*	if (!(result[i_l] = (char*)malloc(sizeof(char) * str_len(s, c, i))))
			return (0);*/
		result[i_l] = ft_strsub(s, i, (size_t)str_len(s, c, i));
		result[i_l][str_len(s, c, (size_t)i)] = '\0';
		i = next_word(s, c, i);
	}
	result[i_l] = NULL;
	return (result);
}
