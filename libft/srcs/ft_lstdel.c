/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:11:22 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/19 12:25:14 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *cpy;

	cpy = *alst;
	if (!alst || !*del)
		return ;
	while (cpy != NULL)
	{
		del(cpy->content, cpy->content_size);
		free(cpy);
		cpy = cpy->next;
	}
	*alst = NULL;
}
