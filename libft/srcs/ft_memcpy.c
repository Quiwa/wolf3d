/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:08:23 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/17 12:54:42 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *str1, const void *str2, size_t n)
{
	char	*ptr;
	char	*ptr2;
	int		i;

	i = -1;
	ptr = (char *)str1;
	ptr2 = (char *)str2;
	while (++i < (int)n)
		ptr[i] = ptr2[i];
	return (str1);
}
