/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_showdoubletable.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 18:12:35 by jlehideu          #+#    #+#             */
/*   Updated: 2018/08/17 10:34:56 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_showdoubletable(char **table)
{
	int	i;

	if (!table)
		return ;
	i = -1;
	while (table[++i])
	{
		if (ft_strcmp(table[i], "\0") == 0)
			return ;
		ft_putstr(table[i]);
		ft_putchar('\n');
	}
}
