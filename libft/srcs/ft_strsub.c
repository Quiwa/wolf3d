/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 11:14:38 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/18 17:37:50 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t lent)
{
	char	*result;
	int		i;
	int		j;
	int		len;

	len = (int)lent;
	if (!(result = (char*)malloc(sizeof(char) * len + 1)) || !s)
		return (0);
	i = start - 1;
	j = -1;
	while (++j < len)
		result[j] = s[++i];
	result[j] = '\0';
	return (result);
}
