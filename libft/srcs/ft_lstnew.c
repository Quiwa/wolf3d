/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <jlehideu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:59:28 by jlehideu          #+#    #+#             */
/*   Updated: 2018/04/19 12:25:06 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*new;

	if (!(new = malloc(sizeof(t_list))))
		return (new);
	if (!(new->content = malloc(sizeof(content))))
		return (new);
	if (!(new->next = malloc(sizeof(content))))
		return (new);
	if (content)
		ft_memcpy(new->content, content, content_size);
	else
		new->content = NULL;
	new->content_size = (content == NULL) ? 0 : content_size;
	new->next = NULL;
	return (new);
}
