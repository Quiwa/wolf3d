/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_3d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 13:47:17 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:37:45 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		print_texture(float x, int y, t_env *e)
{
	int	wall;
	int	idx;
	int hauteur;
	int largeur;

	wall = x * 4 + y * e->a.size_line;
	if (e->text == 9)
		print_pix_wall(x, y, 0x606060, e);
	if (e->texture[e->text].percent_y < 0)
		e->texture[e->text].percent_y = 0;
	if (e->texture[e->text].percent_x < 0)
		e->texture[e->text].percent_x = 0;
	hauteur = (float)e->texture[e->text].percent_y
		* (float)e->texture[e->text].height;
	largeur = (float)(e->texture[e->text].percent_x
			* e->texture[e->text].width);
	idx = (hauteur * e->texture[e->text].size_line) + largeur * 4;
	idx = (e->texture[e->text].size_line * e->texture[e->text].height) - idx;
	if (wall < 0 || wall > WIN_X * WIN_Y * 4 || wall < y * e->a.size_line
		|| wall >= (y + 1) * e->a.size_line || idx < 0
		|| idx >= e->texture[e->text].size_line * e->texture[e->text].height)
		return (print_pix_wall(x, y, 0x000000, e));
	e->a.data[wall] = e->texture[e->text].data[idx];
	e->a.data[wall + 1] = e->texture[e->text].data[idx + 1];
	e->a.data[wall + 2] = e->texture[e->text].data[idx + 2];
}

static void	print_walls_2(int x, int y, float wall_height, t_env *e)
{
	e->texture[e->text].y++;
	if (1 - ((float)e->texture[e->text].y / (float)wall_height + e->lost)
			> 0)
		e->texture[e->text].percent_y = 1 -
		((float)e->texture[e->text].y / (float)wall_height + e->lost);
	if (e->textured_walls == 1)
		print_texture(x, y, e);
	else
		print_pix_wall(x, y, e->color, e);
}

static int	print_walls_3(int x, int y, int wall_top, t_env *e)
{
	if (x < CADRE || x > WIN_Y - CADRE)
	{
		print_pix_wall(x, y, 0x000000, e);
		return (1);
	}
	else if (y < wall_top)
	{
		print_pix_wall(x, y, 0x24445c, e);
		return (1);
	}
	return (0);
}

void		print_walls(int x, t_env *e)
{
	int		w_flo;
	int		w_top;
	float	wall_height;

	e->i = -1;
	w_flo = WIN_Y / 2 + ((WIN_Y * 5) / e->xr.dist) / 2 - (e->angle_y
			* (1 + e->y_sensibility * 1000));
	w_top = WIN_Y / 2 - ((WIN_Y * 5) / e->xr.dist) / 2 - (e->angle_y
			* (1 + e->y_sensibility * 1000));
	wall_height = w_flo - w_top;
	e->lost = (w_top < 1) ? (float)-w_top / (float)wall_height : 0;
	w_top = (w_top < 0) ? 0 : w_top;
	w_flo = (w_flo > WIN_Y) ? WIN_Y : w_flo;
	while (++e->i < WIN_Y)
		if (print_walls_3(x, e->i, w_top, e))
			continue ;
		else if (e->i >= w_top && e->i <= w_flo)
		{
			e->texture[e->text].x += (e->i == w_top || e->i == w_flo) ? 1 : 0;
			e->texture[e->text].y = (e->i == w_top || e->i == w_flo) ? -1
				: e->texture[e->text].y;
			print_walls_2(x, e->i, wall_height, e);
		}
		else if (e->i > w_flo)
			print_pix_wall(x, e->i, 0x333d40, e);
}
