/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 10:46:24 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:07:33 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	manage_axis_left_right(int key, t_env *e)
{
	if ((e->menu_idx > X_LEFT && e->menu_idx <= X_RIGHT
	&& (key == LEFT || key == A)) || (e->menu_idx > Y_LEFT
	&& e->menu_idx <= Y_RIGHT && (key == LEFT || key == A)))
	{
		e->menu_idx--;
		if (e->menu_idx <= Y_RIGHT)
			e->y_sensibility--;
		else
			e->x_sensibility--;
	}
	else if ((e->menu_idx >= X_LEFT && e->menu_idx < X_RIGHT
	&& (key == RIGHT || key == D)) || (e->menu_idx >= Y_LEFT
	&& e->menu_idx < Y_RIGHT && (key == RIGHT || key == D)))
	{
		e->menu_idx++;
		if (e->menu_idx <= Y_RIGHT)
			e->y_sensibility++;
		else
			e->x_sensibility++;
	}
}

static void	manage_axis_up_down(int key, t_env *e)
{
	if ((e->menu_idx >= X_LEFT && e->menu_idx <= X_RIGHT
	&& (key == DOWN || key == S)) || (e->menu_idx >= Y_LEFT
	&& e->menu_idx <= Y_RIGHT && (key == UP || key == W)))
		e->menu_idx = BACK;
	else if ((e->menu_idx == BACK && (key == DOWN || key == S))
	|| (e->menu_idx >= X_LEFT && e->menu_idx <= X_RIGHT &&
		((key == UP || key == W))))
		e->menu_idx = Y_LEFT + e->y_sensibility;
	else if ((e->menu_idx == BACK && (key == UP || key == W))
	|| (e->menu_idx >= Y_LEFT && e->menu_idx <= Y_RIGHT &&
		((key == DOWN || key == S))))
		e->menu_idx = X_LEFT + e->x_sensibility;
}

static int	key_event_menu(int key, t_env *e)
{
	if (key == SELECT)
		e->menu_vue = 1;
	if (e->menu_vue == 1)
	{
		if (key == DOWN || key == S)
			e->menu_idx = (e->menu_idx < 2) ? e->menu_idx + 1 : 0;
		else if (key == UP || key == W)
			e->menu_idx = (e->menu_idx > 0) ? e->menu_idx - 1 : 2;
	}
	else if (e->menu_vue == 2 && (key == W || key == UP
				|| key == S || key == DOWN))
		manage_axis_up_down(key, e);
	else if (e->menu_vue == 2 && (key == A || key == LEFT
				|| key == D || key == RIGHT))
		manage_axis_left_right(key, e);
	menu(key, e);
	return (0);
}

static void	key_event_2(int key, t_env *e)
{
	if (key == MINI_MAP)
	{
		e->opt.mini_map = (e->opt.mini_map == 0) ? 1 : 0;
		e->opt.opacity = (e->opt.opacity == 0) ? 1 : 0;
		e->moved = 1;
	}
	if (key == SHIFT)
		e->speed = (e->speed == 3.0) ? 6.0 : 3.0;
	if (e->moved == 1)
		x_ray(e->mouse_x, e->mouse_y, e);
	if (key == HELP)
	{
		e->help = (e->help == 1) ? 0 : 1;
		e->moved = 1;
	}
}

int			key_event(int key, t_env *e)
{
	e->moved = 0;
	if (key == EXIT)
		exit(EXIT_SUCCESS);
	else if (e->menu_vue || key == SELECT)
		return (key_event_menu(key, e));
	else if (key == TEXTURED)
		e->textured_walls = (e->textured_walls == 1) ? 0 : 1;
	else if (e->ad.y < 0 || e->ad.x < 0)
		return (1);
	else if (key == UP || key == W)
		e->moved = move_up(e);
	else if (key == LEFT || key == A)
		e->moved = move_left(e);
	else if (key == RIGHT || key == D)
		e->moved = move_right(e);
	else if (key == DOWN || key == S)
		e->moved = move_down(e);
	else
		key_event_2(key, e);
	if (e->moved)
		x_ray(e->mouse_x, e->mouse_y, e);
	return (0);
}
