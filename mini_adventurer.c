/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini_adventurer.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 15:27:21 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 22:26:55 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int	mini_adventurer(t_env *e)
{
	double	tmp_x1;
	double	tmp_y1;
	double	tmp_x2;
	double	tmp_y2;

	mini_map(e);
	e->ad.tmp_x = e->ad.x;
	e->ad.tmp_y = e->ad.y;
	e->ad.x2 = e->ad.x + ((double)e->mm.size_x / 5);
	e->ad.y2 = e->ad.y + ((double)e->mm.size_y / 5);
	e->ad.x1 = e->ad.x2 - ((double)e->mm.size_x / 5);
	e->ad.y1 = e->ad.y2 - ((double)e->mm.size_y / 5);
	tmp_x1 = e->ad.x1;
	tmp_y1 = e->ad.y1;
	tmp_x2 = e->ad.x2;
	tmp_y2 = e->ad.y2;
	bresenham_ad(e->ad.x1, e->ad.y1, e);
	e->ad.x2 = tmp_x2;
	e->ad.y2 = tmp_y1;
	e->ad.x1 = tmp_x1;
	e->ad.y1 = tmp_y2;
	bresenham_ad(e->ad.x1, e->ad.y1, e);
	e->ad.x = e->ad.tmp_x;
	e->ad.y = e->ad.tmp_y;
	return (0);
}
