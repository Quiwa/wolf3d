/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 11:57:32 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 15:36:23 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	init_menu_3(t_env *e)
{
	if (!(e->menu[3].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_Yleft.xpm", &e->menu[3].width, &e->menu[3].height)))
		return ;
	if (!(e->menu[2].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_menu_exit.xpm", &e->menu[0].width, &e->menu[0].height)))
		return ;
	if (!(e->menu[1].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_menu_settings.xpm", &e->menu[1].width, &e->menu[1].height)))
		return ;
	if (!(e->menu[0].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_menu_start.xpm", &e->menu[2].width, &e->menu[2].height)))
		return ;
}

static void	init_menu_2(t_env *e)
{
	if (!(e->menu[9].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_back.xpm", &e->menu[9].width, &e->menu[9].height)))
		return ;
	if (!(e->menu[8].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_Xright.xpm", &e->menu[8].width, &e->menu[8].height)))
		return ;
	if (!(e->menu[7].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_Xmid.xpm", &e->menu[7].width, &e->menu[7].height)))
		return ;
	if (!(e->menu[6].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_Xleft.xpm", &e->menu[6].width, &e->menu[6].height)))
		return ;
	if (!(e->menu[5].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_YRight.xpm", &e->menu[5].width, &e->menu[5].height)))
		return ;
	if (!(e->menu[4].addr = mlx_xpm_file_to_image(e->a.mlx,\
	"./menus/WOLF_settings_YMid.xpm", &e->menu[4].width, &e->menu[4].height)))
		return ;
	init_menu_3(e);
}

void		init_menu(t_env *e)
{
	int i;

	i = -1;
	init_menu_2(e);
	while (++i < MENU)
	{
		if (!(e->menu[i].data =\
	(unsigned char *)mlx_get_data_addr(e->menu[i].addr,
	&e->menu[i].bpp, &e->menu[i].size_line, &e->menu[i].endian)))
			return ;
	}
}

static void	enter_menu(t_env *e)
{
	if (e->menu_idx == START)
	{
		e->menu_vue = 0;
		x_ray(e->mouse_x, e->mouse_y, e);
	}
	else if (e->menu_idx == SETTINGS)
	{
		e->menu_vue = 2;
		e->menu_idx = Y_LEFT + e->y_sensibility;
	}
	else if (e->menu_idx == BACK)
	{
		e->menu_vue = 1;
		e->menu_idx = START;
	}
	else if (e->menu_idx == QUIT)
		exit(EXIT_SUCCESS);
}

void		menu(int key, t_env *e)
{
	if (key == ENTER)
		enter_menu(e);
	if (e->menu_vue == 0)
		return ;
	ft_memcpy(e->a.data, e->menu[e->menu_idx].data, e->menu[e->menu_idx].width
			* e->menu[e->menu_idx].height * 4);
	mlx_put_image_to_window(e->a.mlx, e->a.win, e->a.img, 0, 0);
}
