/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_messages.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 17:39:01 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/11 18:41:54 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	exit_m_2(int error)
{
	if (error == E_MALLOC)
	{
		ft_putstr_fd("Error : Malloc error\n", 2);
		exit(EXIT_FAILURE);
	}
	if (error == E_MLX)
	{
		ft_putstr_fd("Error : Graphic library error\n", 2);
		exit(EXIT_FAILURE);
	}
}

void	exit_m(int error)
{
	if (error == E_PARAM)
	{
		ft_putstr_fd("Usage: ./wolf3d param\n", 2);
		exit(EXIT_FAILURE);
	}
	else if (error == E_MAP)
	{
		ft_putstr_fd("Error : Invalid map\n", 2);
		exit(EXIT_FAILURE);
	}
	else if (error == E_EMPTY_MAP)
	{
		ft_putstr_fd("Error : Map is empty\n", 2);
		exit(EXIT_FAILURE);
	}
	else if (error == E_OPEN)
	{
		ft_putstr_fd("Error : Map file seems invalid, please try again\n", 2);
		exit(EXIT_FAILURE);
	}
	else
		exit_m_2(error);
}
