/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham_s.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jlehideu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/07 15:53:25 by jlehideu          #+#    #+#             */
/*   Updated: 2018/12/12 13:46:55 by jlehideu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	axis_1_s(t_env *e)
{
	float	error;

	error = 0;
	while (1)
	{
		if ((int)e->y / (int)e->mm.size_y < 0
				|| (int)e->x / (int)e->mm.size_x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
				|| (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		if (e->opt.opacity)
			print_pix(e);
		error += e->coeff;
		if (e->p.map[(int)e->y / (int)e->mm.size_y]
				[(int)e->x / (int)e->mm.size_x] > '0')
			return (calculate_dist(e));
		if (error >= 0.5)
		{
			e->y -= 0.1;
			error = error - 1;
		}
		e->x -= 0.1;
	}
}

void	axis_2_s(t_env *e)
{
	float	error;

	error = 0;
	while (1)
	{
		if ((int)e->y / (int)e->mm.size_y < 0
				|| (int)e->x / (int)e->mm.size_x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
				|| (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		if (e->opt.opacity)
			print_pix(e);
		error += e->coeff;
		if (e->p.map[(int)e->y / (int)e->mm.size_y]
				[(int)e->x / (int)e->mm.size_x] > '0')
			return (calculate_dist(e));
		if (error >= 0.5)
		{
			e->x -= 0.1;
			error = error - 1;
		}
		e->y -= 0.1;
	}
}

void	axis_3_s(t_env *e)
{
	float	error;

	error = 0;
	while (1)
	{
		if ((int)e->y / (int)e->mm.size_y < 0
				|| (int)e->x / (int)e->mm.size_x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
				|| (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		if (e->opt.opacity)
			print_pix(e);
		error -= e->coeff;
		if (e->p.map[(int)e->y / (int)e->mm.size_y]
				[(int)e->x / (int)e->mm.size_x] > '0')
			return (calculate_dist(e));
		if (error >= 0.5)
		{
			e->x -= 0.1;
			error = error - 1;
		}
		e->y += 0.1;
	}
}

void	axis_4_s(t_env *e)
{
	float	error;

	error = 0;
	while (1)
	{
		if ((int)e->y / (int)e->mm.size_y < 0
				|| (int)e->x / (int)e->mm.size_x < 0)
			return ;
		if ((int)e->y / (int)e->mm.size_y >= e->p.y_len
				|| (int)e->x / (int)e->mm.size_x >= e->p.x_len)
			return ;
		if (e->opt.opacity)
			print_pix(e);
		error -= e->coeff;
		if (e->p.map[(int)e->y / (int)e->mm.size_y]
				[(int)e->x / (int)e->mm.size_x] > '0')
			return (calculate_dist(e));
		if (error >= 0.5)
		{
			e->y += 0.1;
			error = error - 1;
		}
		e->x -= 0.1;
	}
}
